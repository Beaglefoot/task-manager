const requiredField = (value, message = 'Required') => !value && message;

export default requiredField;
