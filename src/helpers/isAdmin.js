const isAdmin = loginState =>
  Boolean(loginState.message.token && loginState.submitted.username);

export default isAdmin;
