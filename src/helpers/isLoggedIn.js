const isLoggedIn = loginState =>
  Boolean(loginState.status === 'ok' && loginState.message.token);

export default isLoggedIn;
