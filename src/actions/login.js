export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';

const TOKEN = 'beejee';

const fakeLogin = ({ username, password }) => dispatch => {
  const action = {
    type: LOGIN_ERROR,
    payload: {
      status: 'error',
      message: {
        username: 'No such user'
      },
      submitted: { username, password }
    }
  };

  if (username === 'admin') {
    if (password === '123') {
      action.type = LOGIN_SUCCESS;
      action.payload.status = 'ok';
      action.payload.message = {
        token: TOKEN
      };

      localStorage.setItem('token', TOKEN);
      localStorage.setItem('username', 'admin');
    } else {
      action.payload.message = {
        password: 'Invalid password'
      };
    }
  }

  return Promise.resolve(dispatch(action));
};

export { fakeLogin as login };
