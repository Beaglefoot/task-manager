export const LOGOUT = 'LOGOUT';

export const logout = () => {
  localStorage.removeItem('token');
  localStorage.removeItem('username');

  return {
    type: LOGOUT
  };
};
