import { sendTask } from '../api';

export const CREATE_TASK_START = 'CREATE_TASK_START';
export const CREATE_TASK_SUCCESS = 'CREATE_TASK_SUCCESS';
export const CREATE_TASK_ERROR = 'CREATE_TASK_ERROR';
export const CREATE_TASK_RESET = 'CREATE_TASK_RESET';

export const createTask = params => async dispatch => {
  dispatch({ type: CREATE_TASK_START });

  let payload;

  try {
    payload = await sendTask(params);
  } catch (error) {
    dispatch({
      type: CREATE_TASK_ERROR,
      payload: {
        status: 'error',
        message: error.message
      }
    });

    throw error;
  }

  dispatch({
    type: payload.status === 'ok' ? CREATE_TASK_SUCCESS : CREATE_TASK_ERROR,
    payload
  });
};

export const createTaskReset = () => ({ type: CREATE_TASK_RESET });
