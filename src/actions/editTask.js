import { updateTask } from '../api';

export const EDIT_TASK_START = 'EDIT_TASK_START';
export const EDIT_TASK_SUCCESS = 'EDIT_TASK_SUCCESS';
export const EDIT_TASK_ERROR = 'EDIT_TASK_ERROR';
export const EDIT_TASK_RESET = 'EDIT_TASK_RESET';

export const editTask = config => async dispatch => {
  dispatch({ type: EDIT_TASK_START });

  let payload;

  try {
    payload = await updateTask(config);
  } catch (error) {
    dispatch({
      type: EDIT_TASK_ERROR,
      payload: {
        status: 'error',
        message: error.message
      }
    });

    throw error;
  }

  dispatch({
    type: payload.status === 'ok' ? EDIT_TASK_SUCCESS : EDIT_TASK_ERROR,
    payload
  });
};

export const editTaskReset = () => ({ type: EDIT_TASK_RESET });
