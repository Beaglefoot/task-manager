import camelize from 'camelize';
import update from 'lodash/update';

import { fetchTasks } from '../api';

export const REQUEST_TASKS_START = 'REQUEST_TASKS_START';
export const REQUEST_TASKS_ERROR = 'REQUEST_TASKS_ERROR';
export const REQUEST_TASKS_SUCCESS = 'REQUEST_TASKS_SUCCESS';

export const requestTasks = config => async dispatch => {
  dispatch({ type: REQUEST_TASKS_START });

  let payload;

  try {
    payload = await fetchTasks(config);
    payload = camelize(payload);
    update(payload, 'message.totalTaskCount', parseInt);
  } catch (error) {
    dispatch({
      type: REQUEST_TASKS_ERROR,
      payload: {
        status: 'error',
        message: error.message
      }
    });

    throw error;
  }

  dispatch({ type: REQUEST_TASKS_SUCCESS, payload });
};
