const initialState = {
  login: {
    status: 'ok',
    message: {
      token: localStorage.getItem('token')
    },
    submitted: {
      username: localStorage.getItem('username'),
      password: null
    }
  },

  tasks: {
    status: 'ok',
    message: {
      tasks: [],
      totalTaskCount: 0
    }
  },

  createTask: {
    status: 'ok',
    message: null
  },

  editTask: {
    status: 'ok',
    message: null
  }
};

export default initialState;
