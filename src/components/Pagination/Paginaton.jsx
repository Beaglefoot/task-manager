import React from 'react';
import { number } from 'prop-types';
import { Link } from 'react-router-dom';
import cn from 'classnames';

import { link, active } from './Pagination.scss';

const Pagination = ({ pageNum = 1, totalPages }) => (
  <div>
    {[...Array(totalPages).keys()].map(num => (
      <Link
        key={num}
        to={`/page/${num + 1}`}
        className={cn(link, pageNum === num + 1 && active)}
      >
        {num + 1}
      </Link>
    ))}
  </div>
);

Pagination.propTypes = {
  pageNum: number,
  totalPages: number.isRequired
};

export default Pagination;
