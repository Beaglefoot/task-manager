import React from 'react';
import { func } from 'prop-types';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import { connect } from 'react-redux';

import TextError from 'components/TextError/TextError';
import { login as loginAction } from 'src/actions/login';
import { loginStatePropType } from 'src/propTypes';

import { login as loginClassName, form } from './Login.scss';

const mapStateToProps = ({ login: loginState }) => ({ loginState });
const mapDispatchToProps = { loginAction };

class Login extends React.Component {
  constructor() {
    super();
    this.validateForm = () => {};
    this.setSubmitting = () => {};
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { loginState } = this.props;

    if (
      loginState.status !== prevProps.loginState.status ||
      loginState.submitted !== prevProps.loginState.submitted
    ) {
      this.setSubmitting(false);
      this.validateForm();
    }
  }

  validateField(field) {
    const { loginState } = this.props;

    return value => {
      if (!value) return 'Required';
      if (
        loginState.status === 'error' &&
        loginState.submitted[field] === value
      ) {
        return loginState.message[field];
      }
    };
  }

  async handleSubmit({ username, password }, { validateForm, setSubmitting }) {
    const { loginAction } = this.props;

    this.validateForm = validateForm;
    this.setSubmitting = setSubmitting;
    await loginAction({ username, password });
  }

  render() {
    return (
      <div className={loginClassName}>
        <h1>Login</h1>
        <Formik
          initialValues={{
            username: '',
            password: ''
          }}
          onSubmit={this.handleSubmit}
        >
          {() => (
            <Form className={form}>
              <label htmlFor="username">
                Username
                <ErrorMessage name="username" component={TextError} />
              </label>
              <Field
                type="text"
                name="username"
                validate={this.validateField('username')}
              />

              <label htmlFor="text">
                Password
                <ErrorMessage name="password" component={TextError} />
              </label>
              <Field
                type="password"
                name="password"
                validate={this.validateField('password')}
              />

              <button type="submit">Submit</button>
            </Form>
          )}
        </Formik>
      </div>
    );
  }
}

Login.propTypes = {
  loginAction: func.isRequired,
  loginState: loginStatePropType
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
