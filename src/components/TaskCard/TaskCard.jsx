import React from 'react';
import { string, number, bool } from 'prop-types';
import { Link } from 'react-router-dom';

import { taskCard, status as statusClassName } from './TaskCard.scss';

const TaskCard = ({ username, email, text, status, id, editable }) => (
  <div className={taskCard}>
    <div>
      <strong>{username}</strong> ({email})
    </div>
    <div>{text}</div>
    <div className={statusClassName}>
      {!!status ? 'Finished' : 'In progress'}
    </div>
    {editable && <Link to={`/edit/${id}`}>Edit</Link>}
  </div>
);

TaskCard.propTypes = {
  username: string.isRequired,
  email: string.isRequired,
  text: string.isRequired,
  status: number.isRequired,
  id: number.isRequired,
  editable: bool
};

export default TaskCard;
