import React from 'react';
import { string, shape } from 'prop-types';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { compose } from 'redux';

import isLoggedIn from 'helpers/isLoggedIn';
import { loginStatePropType } from 'src/propTypes';

import { header, sections, link } from './Header.scss';

const mapStateToProps = ({ login: loginState }) => ({ loginState });

const Header = ({ loginState, location }) => (
  <div className={header}>
    <Link className={link} to="/">
      Home
    </Link>

    {location.pathname !== '/login' && (
      <div className={sections}>
        {isLoggedIn(loginState) ? (
          <Link className={link} to="/logout">
            Logout
          </Link>
        ) : (
          <Link className={link} to="/login">
            Login
          </Link>
        )}
      </div>
    )}
  </div>
);

Header.propTypes = {
  loginState: loginStatePropType,
  location: shape({
    pathname: string.isRequired
  }).isRequired
};

export default compose(
  withRouter,
  connect(mapStateToProps)
)(Header);
