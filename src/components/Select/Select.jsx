import React from 'react';
import { func, string, arrayOf, shape } from 'prop-types';

const Select = ({ onChange, options, label, selectedValue }) => (
  <div>
    <label>{label}</label>
    <select onChange={onChange} value={selectedValue}>
      {options.map(({ value, text }) => (
        <option key={value} value={value}>
          {text}
        </option>
      ))}
    </select>
  </div>
);

Select.propTypes = {
  onChange: func.isRequired,
  options: arrayOf(shape({ value: string, text: string })).isRequired,
  label: string.isRequired,
  selectedValue: string
};

export default Select;
