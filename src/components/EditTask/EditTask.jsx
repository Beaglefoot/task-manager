import React from 'react';
import { string, func } from 'prop-types';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import { connect } from 'react-redux';

import TextError from 'components/TextError/TextError';
import requiredField from 'helpers/requiredField';
import { editTask, editTaskReset } from '../../actions/editTask';
import {
  TASK_STATUS_DONE,
  TASK_STATUS_PENDING,
  loginStatePropType,
  editTaskPropType,
  historyPropType
} from '../../propTypes';

import { editTask as editTaskClassName, form, checkbox } from './EditTask.scss';

const mapStateToProps = ({ login, editTask }) => ({
  loginState: login,
  editTaskState: editTask
});
const mapDispatchToProps = { editTask, editTaskReset };

class EditTask extends React.Component {
  componentDidMount() {
    this.props.editTaskReset();
  }

  componentDidUpdate(prevProps) {
    const { editTaskState } = this.props;
    if (
      prevProps.editTaskState.status === 'pending' &&
      editTaskState.status === 'ok'
    ) {
      this.props.history.push('/');
    }
  }

  render() {
    const { taskId, editTask, loginState } = this.props;

    return (
      <div className={editTaskClassName}>
        <h1>Edit Task {taskId}</h1>
        <Formik
          initialValues={{
            text: '',
            done: false
          }}
          onSubmit={({ text, done }) => {
            const status = done ? TASK_STATUS_DONE : TASK_STATUS_PENDING;
            const { token } = loginState.message;
            editTask({ text, status, taskId, token });
          }}
          render={() => (
            <Form className={form}>
              <label htmlFor="text">
                Description
                <ErrorMessage name="text" component={TextError} />
              </label>
              <Field type="text" name="text" validate={requiredField} />

              <div>
                <label htmlFor="done">Done</label>
                <Field type="checkbox" name="done" className={checkbox} />
              </div>

              <button type="submit">Submit</button>
            </Form>
          )}
        />
      </div>
    );
  }
}

EditTask.propTypes = {
  taskId: string.isRequired,
  editTask: func.isRequired,
  editTaskReset: func.isRequired,
  editTaskState: editTaskPropType,
  loginState: loginStatePropType,
  history: historyPropType
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditTask);
