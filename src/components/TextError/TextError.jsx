import React from 'react';
import { string } from 'prop-types';

import { textError } from './TextError.scss';

const TextError = ({ children }) => <div className={textError}>{children}</div>;

TextError.propTypes = {
  children: string.isRequired
};

export default TextError;
