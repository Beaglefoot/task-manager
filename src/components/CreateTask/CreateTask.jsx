import React from 'react';
import { func } from 'prop-types';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import { validate } from 'email-validator';
import { connect } from 'react-redux';

import TextError from 'components/TextError/TextError';
import requiredField from 'helpers/requiredField';
import { createTask, createTaskReset } from '../../actions/createTask';
import { createTaskPropType, historyPropType } from '../../propTypes';

import { createTask as createTaskClassName, form } from './CreateTask.scss';

const mapStateToProps = ({ createTask }) => ({ createTaskState: createTask });
const mapDispatchToProps = { createTask, createTaskReset };
const validateEmail = email => (validate(email) ? '' : 'Invalid email');

class CreateTask extends React.Component {
  componentDidMount() {
    this.props.createTaskReset();
  }

  componentDidUpdate(prevProps) {
    const { createTaskState } = this.props;
    if (
      prevProps.createTaskState.status === 'pending' &&
      createTaskState.status === 'ok'
    ) {
      this.props.history.push('/');
    }
  }

  render() {
    const {
      createTask,
      createTaskState: { status, message }
    } = this.props;

    if (status === 'error' && typeof message === 'string') {
      return <div>{message}</div>;
    }

    return (
      <div className={createTaskClassName}>
        <h1>Create Task</h1>
        <Formik
          initialValues={{
            username: '',
            email: '',
            text: ''
          }}
          onSubmit={({ username, email, text }) => {
            createTask({ username, email, text });
          }}
          render={({ isSubmitting }) => (
            <Form className={form}>
              <label htmlFor="username">
                Username
                <ErrorMessage name="username" component={TextError} />
              </label>
              <Field type="text" name="username" validate={requiredField} />

              <label htmlFor="email">
                Email
                <ErrorMessage name="email" component={TextError} />
              </label>
              <Field
                type="text"
                name="email"
                validate={email => requiredField(email) || validateEmail(email)}
              />

              <label htmlFor="text">
                Description
                <ErrorMessage name="text" component={TextError} />
              </label>
              <Field type="text" name="text" validate={requiredField} />

              <button type="submit" disabled={isSubmitting}>
                Submit
              </button>
            </Form>
          )}
        />
      </div>
    );
  }
}

CreateTask.propTypes = {
  createTaskState: createTaskPropType,
  createTask: func.isRequired,
  createTaskReset: func.isRequired,
  history: historyPropType
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateTask);
