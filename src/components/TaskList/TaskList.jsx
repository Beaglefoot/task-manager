import React from 'react';
import { number, func } from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { requestTasks } from '../../actions/requestTasks';
import TaskCard from 'components/TaskCard/TaskCard';
import Pagination from 'components/Pagination/Paginaton';
import { tasksPropType, loginStatePropType } from '../../propTypes';
import Select from 'components/Select/Select';
import isAdmin from 'helpers/isAdmin';

import { taskList, selects, addTask } from './TaskList.scss';

const TASKS_PER_PAGE = 3;
const mapStateToProps = ({ tasks, login }) => ({ tasks, loginState: login });
const mapDispatchToProps = { requestTasks };

class TaskList extends React.Component {
  constructor() {
    super();

    this.state = { sortField: 'username', sortDirection: 'asc' };
    this.onSortBySelect = this.onSortBySelect.bind(this);
    this.onSortDirectionSelect = this.onSortDirectionSelect.bind(this);
  }

  requestTasks() {
    const { requestTasks, pageNum } = this.props;
    const { sortField, sortDirection } = this.state;
    requestTasks({ pageNum, sortField, sortDirection });
  }

  componentDidMount() {
    this.requestTasks();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.pageNum !== this.props.pageNum || prevState !== this.state)
      this.requestTasks();
  }

  onSortBySelect(event) {
    this.setState({ sortField: event.target.value });
  }

  onSortDirectionSelect(event) {
    this.setState({ sortDirection: event.target.value });
  }

  render() {
    const { tasks, pageNum, loginState } = this.props;
    const { sortField, sortDirection } = this.state;

    switch (tasks.status) {
      case 'ok':
        return (
          <div className={taskList}>
            <div className={selects}>
              <Select
                onChange={this.onSortBySelect}
                label="Sort by: "
                options={['username', 'email', 'status'].map(option => ({
                  value: option,
                  text: option
                }))}
                selectedValue={sortField}
              />
              <Select
                onChange={this.onSortDirectionSelect}
                label="Sort direction: "
                options={[
                  { value: 'asc', text: 'ascending' },
                  { value: 'desc', text: 'descending' }
                ]}
                selectedValue={sortDirection}
              />
            </div>
            {tasks.message.tasks.map(
              ({ username, email, text, status, id }) => (
                <TaskCard
                  key={id}
                  id={id}
                  username={username}
                  email={email}
                  text={text}
                  status={status}
                  editable={isAdmin(loginState)}
                />
              )
            )}
            <Link className={addTask} to="/create">
              Add Task
            </Link>
            <Pagination
              pageNum={pageNum}
              totalPages={Math.ceil(
                tasks.message.totalTaskCount / TASKS_PER_PAGE
              )}
            />
          </div>
        );
      case 'error':
        return <div>{tasks.message}</div>;
      case 'pending':
        return <div>Loading...</div>;
      default:
        return null;
    }
  }
}

TaskList.propTypes = {
  pageNum: number,
  requestTasks: func.isRequired,
  tasks: tasksPropType,
  loginState: loginStatePropType
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskList);
