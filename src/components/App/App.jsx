import React from 'react';
import { func } from 'prop-types';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Header from 'components/Header/Header';
import TaskList from 'components/TaskList/TaskList';
import CreateTask from 'components/CreateTask/CreateTask';
import EditTask from 'components/EditTask/EditTask';
import Login from 'components/Login/Login';
import isLoggedIn from 'helpers/isLoggedIn';
import isAdmin from 'helpers/isAdmin';
import { logout } from 'src/actions/logout';
import { loginStatePropType } from 'src/propTypes';

import { app, container } from './App.scss';

const mapStateToProps = ({ login }) => ({ loginState: login });
const mapDispatchToProps = { logout };

const App = ({ loginState, logout }) => {
  return (
    <div className={app}>
      <BrowserRouter>
        <Header />
        <div className={container}>
          <Switch>
            <Route exact path="/" component={TaskList} />
            <Route
              exact
              path="/page/:page"
              render={({ match, ...props }) => (
                <TaskList {...props} pageNum={parseInt(match.params.page)} />
              )}
            />
            <Route
              exact
              path="/login"
              render={() =>
                isLoggedIn(loginState) ? <Redirect to="/" /> : <Login />
              }
            />
            <Route exact path="/create" component={CreateTask} />
            <Route
              exact
              path="/edit/:taskId"
              render={({ match, ...props }) =>
                isAdmin(loginState) ? (
                  <EditTask {...props} taskId={match.params.taskId} />
                ) : (
                  <Redirect to="/" />
                )
              }
            />
            <Route
              exact
              path="/logout"
              render={() => {
                logout();
                return <Redirect to="/" />;
              }}
            />
            <Route render={() => 'Not found'} />
          </Switch>
        </div>
      </BrowserRouter>
    </div>
  );
};

App.propTypes = {
  logout: func.isRequired,
  loginState: loginStatePropType
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
