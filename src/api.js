import axios from 'axios';
import qs from 'qs';
import md5 from 'md5';

const API_ROOT = 'https://uxcandy.com/~shapoval/test-task-backend';
const DEVELOPER = 'schernov';

const NETWORK_ERROR_MESSAGE = 'Network error';

export const fetchTasks = async ({
  pageNum = 0,
  sortField = 'id',
  sortDirection = 'asc'
} = {}) => {
  let response;

  try {
    response = await axios.get(
      `${API_ROOT}/?developer=${DEVELOPER}&page=${pageNum}&sort_field=${sortField}&sort_direction=${sortDirection}`
    );

    if (response.data.status === 'error')
      throw new Error(response.data.message);
  } catch (error) {
    throw new Error(NETWORK_ERROR_MESSAGE);
  }

  return response.data;
};

export const sendTask = async ({ username, email, text }) => {
  let response;

  try {
    response = await axios.post(
      `${API_ROOT}/create?developer=${DEVELOPER}`,
      qs.stringify({
        username,
        email,
        text
      }),
      {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }
    );
  } catch (error) {
    throw new Error(NETWORK_ERROR_MESSAGE);
  }

  return response.data;
};

export const updateTask = async ({ text, status, taskId, token }) => {
  const params = { text, status, token };
  const paramsString = Object.keys({ text, status, token })
    .sort()
    .map(key => qs.stringify({ [key]: params[key] }))
    .join('&');

  const signature = md5(paramsString);

  let response;

  try {
    response = await axios.post(
      `${API_ROOT}/edit/${taskId}/?developer=${DEVELOPER}`,
      qs.stringify({
        text,
        status,
        token,
        signature
      }),
      {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }
    );
  } catch (error) {
    throw new Error(NETWORK_ERROR_MESSAGE);
  }

  return response.data;
};
