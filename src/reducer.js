import { produce } from 'immer';

import initialState from './initialState';
import { LOGIN_SUCCESS, LOGIN_ERROR } from './actions/login';
import { LOGOUT } from './actions/logout';
import {
  REQUEST_TASKS_START,
  REQUEST_TASKS_SUCCESS,
  REQUEST_TASKS_ERROR
} from './actions/requestTasks';
import {
  CREATE_TASK_START,
  CREATE_TASK_SUCCESS,
  CREATE_TASK_ERROR,
  CREATE_TASK_RESET
} from './actions/createTask';
import {
  EDIT_TASK_START,
  EDIT_TASK_SUCCESS,
  EDIT_TASK_ERROR,
  EDIT_TASK_RESET
} from './actions/editTask';

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_ERROR:
    case LOGIN_SUCCESS:
      return produce(state, draft => {
        draft.login = action.payload;
      });

    case LOGOUT:
      return produce(state, draft => {
        draft.login.message.token = null;
        draft.login.submitted = {
          username: null,
          password: null
        };
      });

    case REQUEST_TASKS_START:
      return produce(state, draft => {
        draft.tasks.status = 'pending';
      });

    case REQUEST_TASKS_ERROR:
    case REQUEST_TASKS_SUCCESS:
      return produce(state, draft => {
        draft.tasks = action.payload;
      });

    case CREATE_TASK_START:
      return produce(state, draft => {
        draft.createTask.status = 'pending';
      });

    case CREATE_TASK_ERROR:
    case CREATE_TASK_SUCCESS:
      return produce(state, draft => {
        draft.createTask = action.payload;
      });

    case CREATE_TASK_RESET:
      return produce(state, draft => {
        draft.createTask = initialState.createTask;
      });

    case EDIT_TASK_START:
      return produce(state, draft => {
        draft.editTask.status = 'pending';
      });

    case EDIT_TASK_ERROR:
    case EDIT_TASK_SUCCESS:
      return produce(state, draft => {
        draft.editTask = action.payload;
      });

    case EDIT_TASK_RESET:
      return produce(state, draft => {
        draft.editTask = initialState.editTask;
      });

    default:
      return state;
  }
};

export default reducer;
