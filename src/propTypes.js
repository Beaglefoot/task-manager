import {
  shape,
  oneOf,
  oneOfType,
  string,
  number,
  arrayOf,
  object
} from 'prop-types';

export const TASK_STATUS_PENDING = 0;
export const TASK_STATUS_DONE = 10;

export const loginStatePropType = shape({
  status: oneOf(['ok', 'error']).isRequired,
  message: shape({
    username: string,
    password: string
  }),
  submitted: shape({
    username: string,
    password: string
  })
}).isRequired;

export const tasksPropType = shape({
  status: oneOf(['ok', 'error', 'pending']).isRequired,
  message: oneOfType([
    string,
    shape({
      tasks: arrayOf(object).isRequired,
      totalTaskCount: number.isRequired
    })
  ]).isRequired
}).isRequired;

export const createTaskPropType = shape({
  status: oneOf(['ok', 'error', 'pending']).isRequired,
  message: oneOfType([
    string,
    shape({
      id: number.isRequired,
      username: string.isRequired,
      email: string.isRequired,
      text: string.isRequired,
      status: oneOf([TASK_STATUS_PENDING, TASK_STATUS_DONE]).isRequired
    })
  ])
}).isRequired;

export const editTaskPropType = shape({
  status: oneOf(['ok', 'error', 'pending']).isRequired,
  message: string
}).isRequired;

export const historyPropType = object.isRequired;
